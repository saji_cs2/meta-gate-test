## Meta Gate Test

This project is a food ordering

## Used packages

- first used express to build my server and start it up
- next i used mongoose to build the mongodb schema and connect to mongodb
- used multer to handle photo upload there is a file called helper there you will see a lot of utils function
- used express-validation to validate the request before it sent to the controller
- used faker/@faker to produce fake data
- rate limit to limit user requests ideal for ddos
- cors to allow specific origins to make a request but in owr case it is '\*'
- morgan to log our server requests
- bcryptjs to hash user passwords

## How to Use

** To start the server, run: **

- npm install
- change the env MONGODB_URL_Dev value to your mongo DB Url
- the env NODE_ENV = development will enable the stack trace in the api response
- npm run dev will start the server

after starting the server you will need to send 3 in order requests to init the db
request will be :

- /api/v1/seed/categories
- /api/v1/seed/restaurant
- /api/v1/seed/dish

* important to be in order
* after that you can proceed and make a user account (it is ok to make user account before these request)

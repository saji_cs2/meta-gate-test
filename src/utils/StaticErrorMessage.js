/**
 * @description a class that have a static error messages, used to make your life easier **can be configured**
 */
class E {
  static Already = 'duplicate found';
  static somethingWrong = 'Something went wrong, Ops Try again later !';
  static AccessTokenexpired = 'jwt Access Token expired';
  static AccessTokenNotValid = 'Access Token Not Valid ';
  static uploadError = 'Wrong parameter';
  static TokenInvalidSig = 'invalid signature';
  static InvalidFormat = 'Please upload an image formatted by (jpeg,jpg,png)';
  static InvalidSize = 'File too large';
  static FieldRequried = 'Missing parameter';
  static falseCredentials = 'Unable to log in';
  static notFound = 'not found';
  static password =
    'password must contain one lower case characters , one Upper case characters, one numerals, one Special Character,and 8 characters length';
}

export default E;

/* eslint-disable no-unused-vars */
import { errorHandler } from '../middleware/errorHandler.js';
/**
 * @description Common Error class to throw an error from anywhere.
 * The {@link errorHandler} middleware will catch this error at the central place and it will return an appropriate response to the client
 */
class ApiError extends Error {
  // eslint-disable-next-line space-before-function-paren
  constructor(statusCode, message, data = {}, customErrorCode = 0) {
    super(message);
    this.customErrorCode = customErrorCode;
    this.statusCode = statusCode;
    this.data = data;
    this.status =
      statusCode >= 400 && statusCode < 500 ? 'fail' : 'internel server error';
    this.success = false;
    this.isOperational = true;
    Error.captureStackTrace(this, this.constructor);
  }
}

export default ApiError;

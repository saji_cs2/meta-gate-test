/* eslint-disable array-callback-return */
import fs from 'fs';
/* here i write every thing that will help developing like repeated queries
delete images function etc... */

/**
 * @param {import ('express').Request} req
 * @param {String} fileName
 * @param {Boolean} type
 * @description return the file's static path from where they are saved is the server where true indicate to images and false to voices storage
 * @return image url string
 * */
export const getStaticFilePath = (req, fileName, type) => {
  if (type) {
    return `${req.protocol}://${req.get('host')}/public/images/${fileName}`;
  } else {
    return `${req.protocol}://${req.get('host')}/public/voices/${fileName}`;
  }
};
/**
 *
 * @param {string} fileName
 * @param {Boolean} type
 * @description returns the file's local path in the file system to assist future removal
 */
export const getLocalPath = (fileName, type) => {
  if (type) {
    return `public/images/${fileName}`;
  } else {
    return `public/voices/${fileName}`;
  }
};

/**
 *
 * @param {string} localPath
 * @description Removed the local file from the local file system based on the file path
 */

export const removeLocalFile = (localPath) => {
  fs.unlink(localPath, (err) => {
    if (err) console.log('Error while removing local files: ', err);
    else {
      console.log('Removed local: ', localPath);
    }
  });
};

/**
 * @param {import("express").Request} req
 * @description **This utility function is responsible for removing unused image files due to the api fail**.
 *
 * **For example:**
 * * This can occur when product is created.
 * * In product creation process the images are getting uploaded before product gets created.
 * * Once images are uploaded and if there is an error creating a product, the uploaded images are unused.
 * * In such case, this function will remove those unused images.
 */
export const removeUnusedMulterImageFilesOnError = (req) => {
  try {
    const multerFile = req.file;
    const multerFiles = req.files;

    if (multerFile) {
      // If there is file uploaded and there is validation error
      // We want to remove that file
      removeLocalFile(multerFile.path);
    }

    if (multerFiles) {
      const filesValueArray = Object.values(multerFiles);
      // If there are multiple files uploaded for more than one fields
      // We want to remove those files as well
      filesValueArray.map((fileFields) => {
        fileFields.map((fileObject) => {
          removeLocalFile(fileObject.path);
        });
      });
    }
  } catch (error) {
    console.log('Error while removing image files: ', error);
  }
};

/**
 * @param {number} max Ceil threshold (exclusive)
 */
export const getRandomNumber = (max) => {
  return Math.floor(Math.random() * max);
};

import multer from 'multer';
import moment from 'moment';
import ApiError from './ApiError';
import E from './StaticErrorMessage';
const MB = 1024 * 1024;
const date = moment().format('YYYY-MM-DD');

// media storage to upload media files
const mediaStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, './public/images');
  },
  filename: function (req, file, cb) {
    let fileExtension = '';
    if (file.originalname.split('.').length > 1) {
      fileExtension = file.originalname.substring(
        file.originalname.lastIndexOf('.')
      );
    }
    const uniqueSuffix = `${date}-${Math.round(
      Math.random() * 1e9
    )}${fileExtension}`;
    const prefix = req.user?._id || req.employee?._id;
    cb(null, prefix + '-' + uniqueSuffix);
  }
});

export const upload = multer({
  storage: mediaStorage,
  limits: {
    fileSize: 5 * MB
  },
  fileFilter(_req, file, cb) {
    if (!file.originalname.match(/\.(jpeg|jpg|png)$/)) {
      cb(new ApiError(E.InvalidFormat, 400));
    }
    cb(undefined, true);
  }
});

/**
 * @description **build Api Response to keep it consist throw the app**
 * @constructor statuscode: Number, data: any, message: 'success', success: Boolean
 */
class ApiResponse {
  constructor(statusCode, data, message = 'success') {
    this.statusCode = statusCode;
    this.data = data;
    this.message = message;
    this.success = statusCode < 400;
  }
}

export default ApiResponse;

/**
 * @description Every custom error should be putted here (dev configured)
 */
class CustomErrorCode {
  static DuplicatedKey = 11000;
  static AccessTokexpirCode = 10;
  static RefreshTokexpirCode = 11;
}
export default CustomErrorCode;

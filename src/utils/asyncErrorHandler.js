import ApiError from './ApiError.js';
import CustomErrorCode from './CustomErrorCode.js';
import E from './StaticErrorMessage.js';

/**
 *
 * @param {*} func
 * @returns the error passed to the error handler middleware after an instance of class apiError created
 */

export const asyncErrorHandler = (func) => {
  return (req, res, next) => {
    func(req, res, next).catch((err) => {
      console.log(err);
      switch (true) {
        case err.code === CustomErrorCode.DuplicatedKey: {
          const duplicatedValue = new ApiError(409, E.Already);
          return next(duplicatedValue);
        }
        case err.name === 'ValidationError': {
          const validationError = new ApiError(400, err.message);
          return next(validationError);
        }
        case err.name === 'TokenExpiredError': {
          const tokenExpiredError = new ApiError(
            401,
            E.AccessTokenexpired,
            CustomErrorCode.AccessTokexpirCode
          );
          return next(tokenExpiredError);
        }
        case err.message === 'invalid signature': {
          const tokenInvalidSig = new ApiError(
            401,
            E.TokenInvalidSig,
            CustomErrorCode.AccessTokexpirCode
          );
          return next(tokenInvalidSig);
        }
        default:
          return next(err);
      }
    });
  };
};

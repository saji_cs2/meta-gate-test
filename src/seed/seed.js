import { faker } from '@faker-js/faker';
import { Category } from '../model/category.js';
import ApiResponse from '../utils/ApiResponse.js';
import { asyncErrorHandler } from '../utils/asyncErrorHandler.js';
import { Restaurant } from '../model/restaurants.js';
import { getRandomNumber } from '../utils/helper.js';
import { Dish } from '../model/dish.js';

const CATEGORIES_COUNT = 30;
const RESTAURANT_COUNT = 10;
const DISHES_COUNT = 200;

const categories = new Array(CATEGORIES_COUNT).fill('_').map(() => ({
  name: faker.commerce.productAdjective()
}));

const restaurants = new Array(RESTAURANT_COUNT).fill('_').map(() => ({
  email: faker.internet.email(),
  password: faker.internet.password(),
  name: faker.company.name(),
  description: faker.commerce.productDescription(),
  image: {
    url: faker.image.url({ width: 400, height: 400 })
  },
  location: {
    type: 'Point',
    coordinates: [faker.location.longitude(), faker.location.latitude()],
    address: `${faker.location.streetAddress()} ${faker.location.street()} ${faker.location.city()} ${faker.location.state()} ${faker.location.country()}  `
  },
  currency: faker.finance.currencyName()
}));

const dishes = new Array(DISHES_COUNT).fill('_').map(() => ({
  price: faker.commerce.price(),
  name: faker.commerce.productName(),
  addOns: [
    {
      name: faker.commerce.productName(),
      price: faker.commerce.price(),
      isAvailable: getRandomNumber(16) > 8
    },
    {
      name: faker.commerce.productName(),
      price: faker.commerce.price(),
      isAvailable: getRandomNumber(16) > 8
    }
  ],
  variations: [
    {
      size: getRandomNumber(16) > 8 ? 'small' : 'large',
      price: faker.commerce.price(),
      isAvailable: getRandomNumber(100) > 8
    },
    {
      size: getRandomNumber(16) > 8 ? 'small' : 'large',
      price: faker.commerce.price(),
      isAvailable: getRandomNumber(100) > 8
    }
  ],
  isAvailable: getRandomNumber(100) > 8,
  image: {
    url: faker.image.url({ width: 400, height: 400 })
  },
  ingredients: faker.commerce.productDescription(),
  preparingTime: faker.internet.port()
}));

export const seedCategories = asyncErrorHandler(async (req, res, next) => {
  console.log(categories);
  await Category.deleteMany({});
  const result = await Category.insertMany(categories);
  res
    .status(201)
    .send(
      new ApiResponse(
        201,
        { result },
        'Database populated for categories successfully'
      )
    );
});

export const seedRestaurant = asyncErrorHandler(async (req, res, next) => {
  await Restaurant.deleteMany();
  const category = await Category.find();
  const result = await Restaurant.insertMany(
    restaurants.map((restaurant) => ({
      ...restaurant,
      category: [
        category[getRandomNumber(category.length)]?._id,
        category[getRandomNumber(category.length)]?._id
      ]
    }))
  );

  return res
    .status(201)
    .send(
      new ApiResponse(
        201,
        { result },
        'Database populated for restaurants successfully'
      )
    );
});

export const seedDishes = asyncErrorHandler(async (req, res, next) => {
  await Dish.deleteMany();
  const category = await Category.find();
  const restaurant = await Restaurant.find();
  const result = await Dish.insertMany(
    dishes.map((dish) => ({
      ...dish,
      restaurant: restaurant[getRandomNumber(restaurant.length)]?._id,
      category: category[getRandomNumber(category.length)]?._id
    }))
  );
  return res
    .status(201)
    .send(
      new ApiResponse(
        201,
        { result },
        'Database populated for dishes successfully'
      )
    );
});

import { Schema } from 'mongoose';
export const locationSchema = new Schema({
  /* a resuble point schema for the location in every resturant, user and driver  */
  _id: 0,
  type: {
    type: String,
    enum: ['Point']
  },
  coordinates: [{ type: Number }],
  address: {
    type: String
  }
});

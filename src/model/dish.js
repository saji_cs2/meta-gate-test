import mongoose, { Schema } from 'mongoose';
const dishSchema = new Schema(
  {
    name: {
      type: String,
      required: true,
      index: true,
      lowercase: true,
      trim: true
    },
    price: { type: Number, required: true },
    variations: [
      {
        size: {
          type: String,
          required: true
        },
        isAvailable: {
          type: Boolean,
          required: true
        },
        price: {
          type: Number,
          required: true
        }
      }
    ],
    addOns: [
      {
        name: {
          type: String,
          required: true,
          lowercase: true,
          trim: true
        },
        isAvailable: {
          type: Boolean,
          required: true
        },
        price: {
          type: Number,
          required: true
        }
      }
    ],
    isAvailable: {
      type: Boolean,
      required: true,
      default: true
    },
    image: {
      type: {
        url: String,
        localPath: String
      },
      default: {
        url: `https://via.placeholder.com/400x400.png`,
        localPath: ''
      }
    },
    restaurant: {
      type: Schema.Types.ObjectId,
      ref: 'Restaurant',
      required: true
    },
    category: {
      type: Schema.Types.ObjectId,
      ref: 'Category',
      required: true
    },
    ingredients: {
      type: String,
      required: true,
      lowercase: true,
      trim: true
    },
    preparingTime: {
      type: String,
      lowercase: true,
      trim: true
    }
  },
  {
    timestamps: true
  }
);

export const Dish = mongoose.model('Dish', dishSchema);

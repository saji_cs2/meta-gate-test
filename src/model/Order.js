import mongoose from 'mongoose';
import { locationSchema } from './locationSchema.js';
const orderSchema = new mongoose.Schema(
  {
    item: {
      dish: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Dish'
      },
      totalQuantity: {
        type: Number,
        required: true,
        min: 1
      },
      variations: [
        {
          _id: 0,
          size: {
            type: String,
            required: true
          },
          quantity: {
            type: Number,
            required: true
          },
          price: {
            type: Number,
            required: true
          }
        }
      ]
    },
    addOns: [
      {
        _id: 0,
        name: {
          type: String,
          required: true,
          lowercase: true,
          trim: true
        },
        price: {
          type: Number,
          required: true
        },
        quantity: {
          type: Number,
          required: true
        }
      }
    ],
    totalPrice: {
      type: Number,
      required: true
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      required: true
    },
    restaurant: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'Restaurant'
    },
    driver: {
      type: mongoose.Schema.Types.ObjectId,
      // required: true,
      ref: 'Driver'
    },
    to: {
      type: locationSchema,
      required: true
    },
    status: {
      type: String,
      required: true,
      enum: [
        'Pending',
        'Confirmed',
        'Preparing',
        'Out for Delivery',
        'Delivered',
        'Cancelled'
      ]
    },
    note: {
      type: String
    },
    statusDescription: {
      type: String
    },
    deliveryTime: {
      type: String,
      required: true
    }
  },
  {
    timestamps: true
  }
);

export const Order = mongoose.model('Order', orderSchema);

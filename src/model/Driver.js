import mongoose, { Schema } from 'mongoose';
import { locationSchema } from './locationSchema.js';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import ApiError from '../utils/ApiError.js';
import E from '../utils/StaticErrorMessage.js';
const driverSchema = new Schema(
  {
    fname: {
      type: String
    },
    lname: {
      type: String
    },
    location: {
      type: locationSchema,
      required: true,
      index: '2dsphere'
    },
    email: {
      type: String,
      required: true,
      lowercase: true,
      trim: true,
      unique: true
    },
    password: {
      type: String,
      required: true,
      trim: true,
      minlength: 7
    },
    phone: {
      type: String,
      lowercase: true,
      trim: true,
      required: true
    },
    profilePic: {
      type: {
        url: String,
        localPath: String
      },
      default: {
        url: `https://via.placeholder.com/200x200.png`,
        localPath: ''
      }
    },
    birthday: {
      type: Date
    }
  },
  {
    timestamps: true
  }
);

driverSchema.methods.toJSON = function () {
  const driver = this;
  const driverObject = driver.toObject();

  delete driverObject.password;
  delete driverObject.token;
  delete driverObject.__v;

  return driverObject;
};

driverSchema.methods.generateAuthToken = async function () {
  const driver = this;
  const token = jwt.sign(
    {
      _id: driver._id.toString(),
      email: driver.email,
      profilePic: driver.profilePic
    },
    process.env.ACCESS_TOKEN_SECRET
  );
  driver.token = token;
  await driver.save();
  return { token };
};

driverSchema.statics.findBydCredentials = async (email, userName, password) => {
  const driver = await Driver.findOne({ $or: [{ userName }, { email }] });
  if (!driver) {
    throw new ApiError(400, E.falseCredentials);
  }
  const isMatch = await bcrypt.compare(password, driver.password);

  if (!isMatch) {
    throw new ApiError(400, E.falseCredentials);
  }
  return driver;
};

// hash the plain text password befor saving

driverSchema.pre('save', async function (next) {
  const driver = this;
  if (driver.isModified('password')) {
    driver.password = await bcrypt.hash(driver.password, 10);
  }
  next();
});

export const Driver = mongoose.model('Driver', driverSchema);

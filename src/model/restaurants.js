import mongoose, { Schema } from 'mongoose';
import { locationSchema } from './locationSchema.js';
const restaurantSchema = new Schema(
  {
    email: {
      type: String,
      lowercase: true,
      trim: true,
      unique: true
    },
    password: {
      type: String,
      trim: true
    },
    name: {
      type: String,
      required: true
    },
    location: {
      type: locationSchema,
      required: true,
      index: '2dsphere'
    },
    totalRates: {
      type: Number
    },
    numberOfRates: {
      type: Number
    },
    currency: {
      type: String,
      trim: true,
      lowercase: true,
      required: true
    },
    image: {
      type: {
        url: String,
        localPath: String
      },
      default: {
        url: `https://via.placeholder.com/400x400.png`,
        localPath: ''
      }
    },
    description: {
      type: String,
      trim: true,
      lowercase: true
    },
    category: [
      {
        type: Schema.Types.ObjectId,
        required: true
      }
    ]
  },
  {
    timestamps: true
  }
);

restaurantSchema.methods.toJSON = function () {
  const restaurant = this;
  const restaurantObject = restaurant.toObject();
  delete restaurantObject.password;
  delete restaurantObject.__v;

  return restaurantObject;
};

/**
 *
 * @param {Number} longitude
 * @param {Number} latitude
 * @param {Number} minDis
 * @param {String} unit
 * @returns Method return nearby restaurants array sorted ascending to current user location
 *
 */
const findByDistance = function (longitude, latitude, minDis = 0, unit = 'km') {
  const unitValue = unit === 'km' ? 1000 : 1609.3;
  return this.aggregate([
    {
      $geoNear: {
        near: {
          type: 'Point',
          coordinates: [longitude, latitude]
        },
        minDistance: minDis * unitValue,
        distanceField: 'distance',
        distanceMultiplier: 1 / unitValue
      }
    },
    {
      $sort: {
        distance: 1
      }
    },
    {
      $limit: 5
    },
    {
      $project: {
        password: 0,
        email: 0,
        __v: 0
      }
    }
  ]);
};
restaurantSchema.statics = {
  findByDistance
};
export const Restaurant = mongoose.model('Restaurant', restaurantSchema);

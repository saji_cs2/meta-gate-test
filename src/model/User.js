import mongoose, { Schema } from 'mongoose';
import { locationSchema } from './locationSchema.js';
import bcrypt from 'bcryptjs';
import jwt from 'jsonwebtoken';
import ApiError from '../utils/ApiError.js';
import E from '../utils/StaticErrorMessage.js';
const userSchema = new Schema(
  {
    /* user collection to track users accounts */
    email: {
      type: String,
      required: true,
      lowercase: true,
      trim: true,
      unique: true
    },
    password: {
      type: String,
      required: true,
      trim: true,
      minlength: 7
    },
    fname: {
      type: String,
      lowercase: true,
      trim: true
    },
    lname: {
      type: String,
      lowercase: true,
      trim: true
    },
    userName: {
      type: String,
      lowercase: true,
      trim: true,
      unique: true,
      required: true
    },
    phone: {
      type: String,
      lowercase: true,
      trim: true
    },
    location: {
      type: locationSchema,
      index: '2dsphere'
    },
    token: {
      type: String,
      required: true
    },
    profilePic: {
      _id: 0,
      type: {
        url: String,
        localPath: String
      },
      default: {
        url: `https://via.placeholder.com/200x200.png`,
        localPath: ''
      }
    },
    birthday: {
      type: Date
    }
  },
  {
    timestamps: true
  }
);

userSchema.methods.toJSON = function () {
  const user = this;
  const userObject = user.toObject();

  delete userObject.password;
  delete userObject.token;
  delete userObject.__v;

  return userObject;
};

userSchema.methods.generateAuthToken = async function () {
  const user = this;
  const token = jwt.sign(
    {
      _id: user?._id.toString(),
      email: user?.email,
      userName: user?.userName,
      profilePic: user?.profilePic
    },
    process.env.ACCESS_TOKEN_SECRET
  );
  user.token = token;
  await user.save();
  return { token };
};

/**
 *
 * @param {string} email
 * @param {string} password
 * @returns user object
 * @description A static schema method to check user credentials and retrieve it from db
 */
userSchema.statics.findBydCredentials = async (email, userName, password) => {
  const user = await User.findOne({ $or: [{ userName }, { email }] });
  if (!user) {
    throw new ApiError(400, E.falseCredentials);
  }
  const isMatch = await bcrypt.compare(password, user.password);

  if (!isMatch) {
    throw new ApiError(400, E.falseCredentials);
  }
  return user;
};

// hash the plain text password befor saving
userSchema.pre('save', async function (next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 10);
  }
  next();
});

export const User = mongoose.model('User', userSchema);

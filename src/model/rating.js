import mongoose, { Schema } from 'mongoose';
const ratingSchema = new Schema({
  User: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  restaurant: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: 'Restaurant'
  },
  stars: {
    type: Number,
    required: true
  }
});

export const Rate = mongoose.model('Rate', ratingSchema);

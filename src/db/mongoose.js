import { connect } from 'mongoose';
const serverSelectionTimeoutMS = 5000;
const connectDb = async () => {
  for (let i = 0; i < 3; ++i) {
    try {
      // note if you want to connect to another DB change the env variable
      await connect(process.env.MONGODB_URL_Dev, {
        serverSelectionTimeoutMS
      });
      console.log('Connection to database success');
      break;
    } catch (error) {
      console.log(
        `connection attempt ${i + 1} to DB faild after ${
          serverSelectionTimeoutMS / 1000
        } second`
      );
      if (i >= 2) {
        console.log(
          `Connection to Database faild after ${i + 1} attempts and ${
            (serverSelectionTimeoutMS / 1000) * 3
          } second (check for vpn if connected disable it ),`
        );
      }
    }
  }
};
export default connectDb;

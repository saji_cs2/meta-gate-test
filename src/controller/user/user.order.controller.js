import { Dish } from '../../model/dish.js';
import { Order } from '../../model/Order.js';
import ApiError from '../../utils/ApiError.js';
import ApiResponse from '../../utils/ApiResponse.js';
import { asyncErrorHandler } from '../../utils/asyncErrorHandler.js';

export const addOrder = asyncErrorHandler(async (req, res, next) => {
  const { dishId, variations, addOnIds, restaurantId, to, note } = req.body;
  const user = req.user._id;
  let totalPrice = 0;
  const selectedAddOns = [];
  const selectedVariation = [];
  let totalQuantity = 0;
  if (!dishId) {
    throw new ApiError(400, 'please provide a dish Id');
  }
  const selectedDish = await Dish.findById(dishId);
  if (!selectedDish) {
    throw new ApiError(404, 'dish not found');
  }
  if (!selectedDish.isAvailable) {
    throw new ApiError(404, 'Dish not available');
  }
  if (addOnIds) {
    // for loop to iterate throw dish addons and select the ones that the user selected throw its id
    for (const addonId of addOnIds) {
      let tempPrice = 0;
      let tempQuantity = 0;
      const match = selectedDish.addOns.find(
        (addon) => Object.keys(addonId).toString() === addon._id.toString()
      );
      console.log(match);
      if (!match || !match.isAvailable) {
        throw new ApiError(400, 'addOns is not available try again later');
      }
      const structMatch = {
        price: match.price,
        name: match.name,
        quantity: parseInt(Object.values(addonId))
      };
      selectedAddOns.push(structMatch);
      tempQuantity += parseInt(Object.values(addonId));
      tempPrice += match.price * tempQuantity;
      totalPrice += tempPrice;
    }
    console.log(totalPrice);
  }
  if (variations) {
    // for loop to iterate throw dish variation and select picked ones throw id
    for (const variationId of variations) {
      let tempPrice = 0;
      let tempQuantity = 0;
      const match = selectedDish.variations.find(
        (variation) =>
          variation._id.toString() === Object.keys(variationId).toString()
      );
      if (!match || !match.isAvailable) {
        throw new ApiError(400, 'variations is not available try again later');
      }
      const structMatch = {
        size: match.size,
        price: match.price,
        quantity: parseInt(Object.values(variationId))
      };
      selectedVariation.push(structMatch);
      tempQuantity += parseInt(Object.values(variationId));
      tempPrice += match.price * tempQuantity;
      totalPrice += tempPrice;
      totalQuantity += tempQuantity;
    }
  }
  // structure item
  const item = {
    dish: selectedDish?._id,
    price: selectedDish?.price,
    totalQuantity,
    variations: selectedVariation
  };

  const order = await Order.create({
    item,
    addOns: selectedAddOns,
    user,
    restaurant: restaurantId,
    totalPrice,
    to,
    status: 'Pending',
    deliveryTime: selectedDish.preparingTime,
    note: note || ''
  });
  return res.status(201).send(new ApiResponse(201, { order }, 'success'));
});

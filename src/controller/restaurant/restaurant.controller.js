import mongoose from 'mongoose';
import { Restaurant } from '../../model/restaurants.js';
import ApiError from '../../utils/ApiError.js';
import ApiResponse from '../../utils/ApiResponse.js';
import { asyncErrorHandler } from '../../utils/asyncErrorHandler.js';
const ObjectId = mongoose.Types.ObjectId;

const restaurantCommonAggregation = (page, limit) => {
  return [
    {
      $lookup: {
        from: 'dishes',
        localField: '_id',
        foreignField: 'restaurant',
        as: 'dish',
        pipeline: [
          {
            $lookup: {
              from: 'categories',
              localField: 'category',
              foreignField: '_id',
              as: 'categories'
            }
          },
          {
            $project: {
              restaurant: 0,
              category: 0,
              __v: 0
            }
          }
        ]
      }
    },
    {
      $project: {
        dish: 1
      }
    },
    {
      $unwind: '$dish' // Unwind dishes array to allow per-dish operations
    },
    {
      $facet: {
        data: [
          {
            $skip: (page - 1) * limit // Calculate skip based on page and limit
          },
          {
            $limit: limit // Limit the number of dishes returned per page
          }
        ],
        totalCount: [{ $count: 'total' }] // Count total dishes for pagination info
      }
    },
    {
      $project: {
        page: '$data',
        totalDishes: { $arrayElemAt: ['$totalCount', 0] }
      }
    }
  ];
};
export const NearByRestaurants = asyncErrorHandler(async (req, res, next) => {
  const { longitude, latitude, minDis } = req?.body;
  if (!longitude && !latitude && !minDis) {
    throw new ApiError(400, 'please provide a valid location and minDis');
  }
  // used a self written (findByDistance) method to fetch nearby restaurants explained in restaurant schema
  const NearByRestaurants = await Restaurant.findByDistance(
    longitude,
    latitude,
    minDis
  );
  return res.status(200).send(new ApiResponse(200, { NearByRestaurants }));
});

export const restaurantDishes = asyncErrorHandler(async (req, res, next) => {
  const page = parseInt(req.query?.page) || 1;
  const limit = parseInt(req.query?.limit) || 10;
  if (!req.body?.restId) {
    throw new ApiError(400, 'please provide a restaurant id');
  }
  const dishes = await Restaurant.aggregate([
    {
      $match: {
        _id: new ObjectId(req.body?.restId)
      }
    },
    ...restaurantCommonAggregation(page, limit)
  ]);
  const restDishes = dishes[0];
  if (!restDishes) {
    throw new ApiError(404, 'restaurant with dishes not found');
  }
  return res.send(new ApiResponse(200, restDishes));
});

import { User } from '../../model/User.js';
import ApiError from '../../utils/ApiError.js';
import ApiResponse from '../../utils/ApiResponse.js';
import { asyncErrorHandler } from '../../utils/asyncErrorHandler.js';

export const signUp = asyncErrorHandler(async (req, res, next) => {
  console.log(req.body);
  const { email, password, userName, location } = req?.body;

  const existedUser = await User.findOne({
    $or: [{ userName }, { email }]
  });

  if (existedUser) {
    throw new ApiError(409, 'User with email or username already exists', []);
  }

  const user = new User({
    email,
    password,
    userName,
    location
  });

  const { token } = await user.generateAuthToken();

  res
    .status(201)
    .send(
      new ApiResponse(201, { user, token }, 'User account created successfully')
    );
});

export const login = asyncErrorHandler(async (req, res, next) => {
  const { email, username, password } = req?.body;

  if (!username && !email) {
    throw new ApiError(400, 'Username or email is required');
  }
  const user = await User.findBydCredentials(email, username, password);
  const { token } = await user.generateAuthToken();

  res
    .status(200)
    .send(new ApiResponse(200, { user, token }, 'User logged successfully'));
});

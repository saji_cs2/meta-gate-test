import { Driver } from '../../model/Driver.js';
import ApiError from '../../utils/ApiError.js';
import ApiResponse from '../../utils/ApiResponse.js';
import { asyncErrorHandler } from '../../utils/asyncErrorHandler.js';

export const signUp = asyncErrorHandler(async (req, res, next) => {
  const { email, password, userName, phone, location } = req?.body;
  const existedDriver = await Driver.findOne({
    $or: [{ userName }, { email }]
  });

  if (existedDriver) {
    throw new ApiError(409, 'Driver with email or username already exists', []);
  }

  const driver = new Driver({
    email,
    password,
    phone,
    userName,
    location
  });

  const { token } = await driver.generateAuthToken();

  res
    .status(201)
    .send(
      new ApiResponse(
        201,
        { driver, token },
        'driver account created successfully'
      )
    );
});

export const login = asyncErrorHandler(async (req, res, next) => {
  const { email, username, password } = req?.body;

  if (!username && !email) {
    throw new ApiError(400, 'Username or email is required');
  }
  const driver = await Driver.findBydCredentials(email, username, password);
  const { token } = await driver.generateAuthToken();

  res
    .status(200)
    .send(new ApiResponse(200, { driver, token }, 'User logged successfully'));
});

import { removeUnusedMulterImageFilesOnError } from '../utils/helper.js';
/**
 *
 * @param {Error | ApiError} err
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @param {import("express").NextFunction} next
 *
 *
 * @description This middleware is responsible to catch the errors from any request handler wrapped inside the {@link asyncHandler}
 */
export const errorHandler = (error, req, res, next) => {
  error.customErrorCode = error.customErrorCode || 0;
  error.statusCode = error.statusCode || 500;
  error.status = error.status || 'Something Went Wrong';
  error.data = error.data || '';
  console.log(error);
  removeUnusedMulterImageFilesOnError(req);
  return res.status(error.statusCode).send({
    statusCode: error.statusCode,
    customErrorCode: error.customErrorCode,
    errorMessage: error.message,
    data: error.data,
    RequestStatus: error.status,
    success: error.success,
    ...(process.env.NODE_ENV === 'development' ? { stack: error.stack } : {}) // Error stack traces should be visible in development for debugging
  });
};

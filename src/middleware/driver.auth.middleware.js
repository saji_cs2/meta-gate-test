import { Driver } from '../model/Driver.js';
import ApiError from '../utils/ApiError.js';
import { asyncErrorHandler } from '../utils/asyncErrorHandler.js';
import jwt from 'jsonwebtoken';

export const verifyJWT = asyncErrorHandler(async (req, res, next) => {
  const token =
    req.cookies?.accessToken ||
    req.header('Authorization')?.replace('Bearer ', '');

  if (!token) {
    throw new ApiError(401, 'Unauthorized request');
  }

  const decodedToken = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET);
  const driver = await Driver.findById(decodedToken?._id);
  if (!driver) {
    throw new ApiError(401, 'Invalid access token');
  }
  req.driver = driver;
  next();
});

import express from 'express';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import connectDb from './db/mongoose.js';
import { errorHandler } from './middleware/errorHandler.js';
import { config } from 'dotenv';
import userRouter from './routes/auth/user.routes.js';
import driverRouter from './routes/auth/driver.routes.js';
import seedRoute from './routes/seed/seeder.js';
import restaurantRouter from './routes/restaurant/restaurant.routes.js';
import orderRouter from './routes/order/order.routes.js';
import ApiError from './utils/ApiError.js';
import fs from 'fs';
import rateLimit from 'express-rate-limit';
import path, { dirname } from 'path';
import { fileURLToPath } from 'url';
import morgan from 'morgan';
config();
await connectDb();

const app = express();
const PORT = 3000 || process.env.PORT;

app.use(
  cors({
    origin: '*'
  })
);
// Rate limiter to avoid misuse of the service and avoid cost spikes
const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 500, // Limit each IP to 500 requests per `window` (here, per 15 minutes)
  standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
  legacyHeaders: false, // Disable the `X-RateLimit-*` headers
  handler: (_, __, ___, options) => {
    throw new ApiError(
      options.statusCode || 500,
      `There are too many requests. You are only allowed ${
        options.max
      } requests per ${options.windowMs / 60000} minutes`
    );
  }
});

const currentDir = dirname(fileURLToPath(import.meta.url));
const morganOptions =
  ':remote-addr - :remote-user [:date[iso]] :method :url :status :res[content-length]  - :response-time ms ';
const accessLogStream = fs.createWriteStream(
  path.join(currentDir, '/logs/server.log'),
  {
    flags: 'a'
  }
);

app.use(morgan(morganOptions, { stream: accessLogStream }));
app.use(express.json());
app.use(cookieParser());
app.use(limiter);
app.use('/api/v1/user', userRouter);
app.use('/api/v1/driver', driverRouter);
app.use('/api/v1/order', orderRouter);
app.use('/api/v1/restaurant', restaurantRouter);
app.use('/api/v1/seed', seedRoute);
app.use(errorHandler);

app.listen(PORT, () => {
  console.log(`server is up and running on http://localhost:${PORT}`);
});

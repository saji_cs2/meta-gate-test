import { Router } from 'express';
import { login, signUp } from '../../controller/auth/user.auth.js';
import {
  userLoginValidator,
  userRegisterValidator
} from '../../validators/auth/user.validators.js';
import { validate } from '../../validators/validate.js';

const router = Router();

router.route('/register').post(userRegisterValidator(), validate, signUp);
router.route('/login').post(userLoginValidator(), validate, login);
export default router;

import { Router } from 'express';
import { verifyJWT } from '../../middleware/user.auth.middleware.js';
import {
  NearByRestaurants,
  restaurantDishes
} from '../../controller/restaurant/restaurant.controller.js';
import { validate } from '../../validators/validate.js';
import { mongoIdRequestBodyValidator } from '../../validators/common/mongoDb.js';

const router = Router();
router.use(verifyJWT);

router.route('/nearby').get(NearByRestaurants);
router
  .route('/dishes')
  .get(mongoIdRequestBodyValidator('restId'), validate, restaurantDishes);

export default router;

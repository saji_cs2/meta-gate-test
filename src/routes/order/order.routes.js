import { Router } from 'express';
import { addOrder } from '../../controller/user/user.order.controller.js';
import { verifyJWT } from '../../middleware/user.auth.middleware.js';
const router = Router();

router.use(verifyJWT);
router.route('/new').post(addOrder);

export default router;

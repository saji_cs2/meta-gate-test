import { seedCategories, seedDishes, seedRestaurant } from '../../seed/seed.js';
import { Router } from 'express';
const route = Router();

route.route('/categories').post(seedCategories);
route.route('/restaurant').post(seedRestaurant);
route.route('/dish').post(seedDishes);
export default route;
